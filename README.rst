hex2words
=========

`hex2words <https://pypi.python.org/pypi/hex2words>`__ is a command-line python program to convert hexadecimal
numbers (e.g. file hashes, GPG keys fingerprints, etc.) into a list of
PGP words.

These PGP words can be used then to check if fingerprints or hashes
match via a phone line or just reading them aloud to your workmates.

PGP words
=========

`PGP words <https://en.wikipedia.org/wiki/PGP_word_list>`__ are words
chosen in a way that they sound very different to each other. Each
number between 0 and 255 has two equivalent words, for example 0x12 has
*atlas* and *backwater*.

If the byte 0x12 is in even order (2nd position, 4th, etc.) will be
translated to *atlas*, if it is in odd order, it will be translated to
*backwater*. In this way, if you have a number like 0x1212 you will get
two different words: *atlas backwater*, instead of twice the same word,
which prevents some mistakes of being done via a phone conversation.

hex2words for nodejs
====================
This `hex2words <https://www.npmjs.com/package/hex2words>`__ is a library for *npm*, in case you are interested in NodeJS. Not mine.

Using hex2words
===============

Installing hex2words
--------------------
::

  $ pip install hex2words

Simplest usage
--------------

The words will be always all in lower-case. It seems, the lower or upper case changes the intonation when using programs like `festival` or `say`.

With 3 identifiers, a MAC, an IP6 address and some custom-format hexa id. The three as command-line arguments::

  $ hex2words dc:0e:e5:62:94:ac fe80::df0e:e5ff:fe62:94ac 920d6b30-0fa4-11e5-9cde-442a60f31a14
  dc:0e:e5:62:94:ac
          sweatband atlantic topmost gadgetry pluto penetrate
  
  fe80::df0e:e5ff:fe62:94ac
          woodlark intention talon atlantic topmost yucatán woodlark gadgetry pluto penetrate
  
  920d6b30-0fa4-11e5-9cde-442a60f31a14
          physique asteroid glitter commando artist pandora athens travesty python telephone crumpled chambermaid facial vertigo beehive belowground
  $

Piping file contents
--------------------

The same three identifiers as before, stored in a file::

  $ cat > test
  dc:0e:e5:62:94:ac
  fe80::df0e:e5ff:fe62:94ac
  920d6b30-0fa4-11e5-9cde-442a60f31a14
  ^D
  $

And then, piping the file::

  $ cat test | hex2words
  dc:0e:e5:62:94:ac
          sweatband Atlantic topmost gadgetry Pluto penetrate
  
  fe80::df0e:e5ff:fe62:94ac
          woodlark intention talon Atlantic topmost Yucatán woodlark gadgetry Pluto penetrate
  
  920d6b30-0fa4-11e5-9cde-442a60f31a14
          physique asteroid glitter commando artist Pandora Athens travesty python telephone crumpled chambermaid facial vertigo beehive belowground
  $


Piping file hashes
------------------
SHA1 hashes of all python source files in the hex2words/ directory::

  $ sha1sum hex2words/*.py  | hex2words
  hex2words/__init__.py:
          2a8c85a1f694f79bda523b1423a3f7a473824fc5
          brickyard megaton music outfielder village molecule virus Norwegian surmount enrollment clockwork belowground blowtorch pandemic virus Pandora hockey Istanbul dropper resistor
  
  hex2words/hex2words.py:
          6d859a1b70ee1f8274555031df14522cbb2137a0
          goggles leprosy pupil bravado guidance universe billiard Istanbul indoors equipment drumbeat company talon belowground Dupont Chicago shamrock Camelot clamshell Orlando
  
  hex2words/input.py:
          769fb587669dfc506ad5b951ac05bd6379f2386a
          inverse opulent scorecard liberty framework Ohio wayside embezzle Geiger specialist sentence enchanting ribcage almighty skullcap Galveston jawbone vagabond classic hamburger
  
  hex2words/program.py:
          d1bd5a1445be18880a842eb62bd15ab28cc39d35
          stairway quantity enlist belowground crusade racketeer beaming maritime allow Jupiter buzzard potato briefcase scavenger enlist pioneer offload replica quadrant conformist
  
  hex2words/version.py:
          b6ba6a706b082a8b440150f1ee2f229bcaa1d658
          Scotland puberty Geiger hesitate glitter antenna brickyard Medusa crumpled adviser drumbeat vacancy tycoon combustion blockade Norwegian spellbind outfielder stockman everyday
  $

Piping GPG fingerprints
-----------------------
You can use both, the last 4 bytes of the fingerprint (*34EC9CBA*), or the complete fingerprint::

  $ hex2words 34EC9CBA "EF45 5106 80FB 0232 6B04 5AFB 3247 4CF8 34EC 9CBA"
  34EC9CBA
          choking unicorn python puberty
  
  EF45 5106 80FB 0232 6B04 5AFB 3247 4CF8 34EC 9CBA
          uncut detector drunken amulet merit Wichita accrue component glitter alkali enlist Wichita checkup determine drainage warranty choking unicorn python puberty

Or piping from *gpg*::

  $ gpg --list-keys --fingerprint 34EC9CBA | hex2words
  EF45510680FB02326B045AFB32474CF834EC9CBA: uncut detector drunken amulet merit Wichita accrue component glitter alkali enlist Wichita checkup determine drainage warranty choking unicorn python puberty

Development
===========

Installing for development
--------------------------
::

  $ git clone git@bitbucket.org:pfigue/hex2words.git
  $ cd hex2words/

then you can run ``setup.py install``, for example.

Running the tests
-----------------

``setup.py test`` should work. Tests are written for py.test framework. They live in hex2words/tests/.

Contributing
------------

Just fork the project and send pull requests with your magic. Try to
indent your code with ``flake8`` or similar, and try to write tests and
nice code.

All feedback is welcomed!
-------------------------
