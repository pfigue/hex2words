# Tools used to develop and/or prepare the package.

# Indentation
flake8
pylint

# Automated testing
pytest
tox

# Packaging
setuptools
twine

# You may want to install also pypy or pypy3 if you want
# to use it instead of cpython.
